drop database if exists  `ret_database`;
create database `ret_database`;
use `ret_database`;

set names utf8;
set character_set_client = utf8mb4;

# drop table if exists tbl_localidades;

create table tbl_localidades (
Id_localidad tinyint(5) not null auto_increment,
region varchar(10) not null,
provincia varchar(100) not null,
departamento varchar(50),
ciudad varchar(100),
institucion varchar(100),
especificacion varchar(100),
lat	dec(12,10),
lon dec(12,10),
contacto varchar(150),
primary key(Id_localidad)
);

load data infile "/var/lib/mysql-files/RET/tbl_localidades.csv" into table tbl_localidades
FIELDS TERMINATED BY ','
enclosed by " "
LINES TERMINATED BY '\n'
IGNORE 1 LINES (region, provincia, departamento, ciudad, institucion, especificacion,lat, lon, contacto);

#insert into  tbl_localidades (region, provincia, departamento, ciudad, institucion, especificacion,lat, lon, contacto)
#values ("I","Santa Fe","Castellanos","Rafaela","INTA","EEA Rafaela- INTA","-31.1962064427","-61.495337902","Carlos, Bainotti (bainotti.carlos@inta.gob.ar)");

create table tbl_ensayos (
Id_Ensayo int not null auto_increment,
Campagna char(4),
Condicion enum('Regular', 'Alta Tecnologia'), # 1 = regular 2 = alta tecnologia
Disegno char(6), 
FS_Largo date, ## formato %y %m %d
FS_LargoIntermedio date,
FS_CortoIntermedio date,
FS_Corto date,
Descripcion text(255),
Id_localidad tinyint(5),
primary key(Id_Ensayo),
key `FK_Id_localidad` (`Id_localidad`),
constraint SYS_FK_74 foreign key(`Id_localidad`) references `tbl_localidades`(`Id_localidad`)
);

load data infile "/var/lib/mysql-files/RET/tbl_ensayos.csv" into table tbl_ensayos
FIELDS TERMINATED BY ','
enclosed by " "
LINES TERMINATED BY '\n'
IGNORE 1 LINES (Campagna,Condicion,Disegno,FS_Largo,FS_LargoIntermedio,FS_CortoIntermedio,FS_Corto, Descripcion,Id_localidad);


create table tbl_cultivares (
	Id_Cultivar int not null auto_increment,
    Nombre varchar(50),
    Propietario varchar(50),
    Alta Date,
    Ciclo char(2),
    Grupo varchar(10),
    primary key(Id_Cultivar)
    );

load data infile "/var/lib/mysql-files/RET/tbl_cultivares.csv" into table tbl_cultivares
fields terminated by ','
enclosed by " " 
lines terminated by '\n'
ignore 1 lines (Nombre, Propietario, Alta, Ciclo);


/*
### exporta tabla a csv para poder emplear las claves primarias

select 
	Id_cultivar, Nombre
from
	tbl_cultivares
into outfile "/var/lib/mysql-files/tbl_cultivares.csv"
fields terminated by ','
enclosed by " "
lines terminated by '\n'
;

select 
	Id_Ensayo, Descripcion
from 
	tbl_ensayos
into outfile
	"/var/lib/mysql-files/tbl_ensayos"
fields terminated by ','
enclosed by " "
lines terminated by '\n'
;   
*/
 
drop table tbl_rendimiento;

create table tbl_rendimiento (
	Id_Registro  int not null auto_increment,
    Ciclo varchar(20),
    Tratamiento char(2),
    Repeticion tinyint(2),
    Rendimiento dec(10,2),
    Id_Cultivar int,
    Id_Ensayo int,
primary key(Id_Registro),
key `FK_Id_Cultivar`(`Id_Cultivar`),
constraint foreign key(`Id_Cultivar`) references `tbl_cultivares`(`Id_Cultivar`),
key `FK_Id_Ensayo`(`Id_Ensayo`),
constraint foreign key(`Id_Ensayo`) references `tbl_ensayos`(`Id_Ensayo`)
);

load data infile "/var/lib/mysql-files/RET/tbl_rendimiento2.csv" into table tbl_rendimiento
fields terminated by ','
enclosed by " " 
lines terminated by '\n'
ignore 1 lines (Ciclo, Tratamiento, Repeticion, Rendimiento, Id_Cultivar,Id_Ensayo);
