---
title: "Guía Práctica"
author: "Cesar Gastón Torres"
date: "4/3/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

---
title: "Tutor"
output: html_notebook
---

### Cargar librerias  

```{r }
library(DBI)
library(odbc)
```


### Conexión
```{r}
con <- DBI::dbConnect(
                      odbc::odbc(),
                      driver = "MySQL",
                      database = "byo7idkx5pni1l1uiket", 
                      UID    = "usdqgqyhajy8ahh1",
                      PWD    =  "0qWVcphp1FgqSuBTUyS3",
                      server = "byo7idkx5pni1l1uiket-mysql.services.clever-cloud.com",
                      port = 3306
                      )
```


#### REALIZAR CONSULTA  
Meta: explorar el objeto [con]


#### Mostrar Tablas 
Explorar la Base de Datos para conocer los componentes esenciales de la misma.


#### Describir Tablas

Emplear las funciones del paquete DBI para explorar las tablas a fin de conocer sus campos y el formato de los mismos.


#### Obtener Target 1
Meta: generar un conjunto de datos que tome de tbl_localidades ciudad y coordenadas, de tbl_ensayos Id_Ensayos, Campagna, Condicion y FS_Corto  


#### Mediante dplyr

Explorar la función tbl y emplearla para reproducir el resultado anterior

#### Escribiendo código SQL 

Meta: generar una tabla con datos de rendimiento por cultivares, tratamiento, repeticion, id_ensayo, condicion, Campagna, coordenadas, ciudad cuando el ciclo es Largo.


```{sql, connection=con, output.var = "mydataframe"}


```


```{r}
mydataframe
```

